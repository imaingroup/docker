server {
    listen 80 default_server;
    listen [::]:80 default ipv6only=on;

    auth_basic "Login to protected area";
    auth_basic_user_file /run/secrets/insider_htpasswd;

    server_name _;

    location /api/external/imperium/version {
        root /app/public;
        try_files index.php?$query_string /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass insider_fpm:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_intercept_errors    on;
        fastcgi_ignore_client_abort off;
        fastcgi_connect_timeout     3s;
        fastcgi_buffer_size         128k;
        fastcgi_buffers             128 16k;
        fastcgi_busy_buffers_size   256k;
        fastcgi_temp_file_write_size 256k;
        reset_timedout_connection on;
    }

    location / {
        return 301 https://deimos3.space$request_uri;
    }
}

server {
    listen 443 ssl;

    server_name www.deimos3.space 104.219.251.120;

    auth_basic "Login to protected area";
    auth_basic_user_file /run/secrets/insider_htpasswd;

    ssl_certificate /run/secrets/insider_ssl.crt;
    ssl_certificate_key /run/secrets/insider_ssl.key;

    return 301 https://deimos3.space$request_uri;
}

server {
    listen 443 ssl;

    server_name deimos3.space;
    index index.html;
    charset utf-8;

    auth_basic "Login to protected area";
    auth_basic_user_file /run/secrets/insider_htpasswd;

    location ~ ^/(storage/|assets/|js-854/|vendor/) {
        root /app/public;
        add_header Cache-Control "max-age=1800, must-revalidate";
        try_files $uri $uri/ /index.html =404;
    }

    location ~ ^(/api) {
        root /app/public;
        try_files index.php?$query_string /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass insider_fpm:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_intercept_errors    on;
        fastcgi_ignore_client_abort off;
        fastcgi_connect_timeout     10s;
        fastcgi_buffer_size         128k;
        fastcgi_buffers             128 16k;
        fastcgi_busy_buffers_size   256k;
        fastcgi_temp_file_write_size 256k;
        reset_timedout_connection on;
    }

    location ~ ^(/XI400PGNB|/_debugbar) {
        proxy_read_timeout 300;
        proxy_connect_timeout 300;
        proxy_send_timeout 300;

        client_max_body_size 2048m;

        root /app/public;
        try_files index.php?$query_string /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass insider_fpm:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_intercept_errors    on;
        fastcgi_ignore_client_abort off;
        fastcgi_buffer_size         128k;
        fastcgi_buffers             128 16k;
        fastcgi_busy_buffers_size   256k;
        fastcgi_temp_file_write_size 256k;
        reset_timedout_connection on;
        fastcgi_connect_timeout 10s;
        fastcgi_send_timeout 300s;
        fastcgi_read_timeout 300s;
    }

    location / {
        root /usr/share/nginx/html;
        try_files $uri /index.html =404;
    }

    ssl_certificate /run/secrets/insider_ssl.crt;
    ssl_certificate_key /run/secrets/insider_ssl.key;

    ssl_dhparam /etc/nginx/dhparam;
    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;
    ssl_session_tickets off;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload" always;
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-XSS-Protection "1; mode=block" always;

    error_log /var/log/nginx/insider-error.log notice;
    access_log /var/log/nginx/insider-access.log main;
}
