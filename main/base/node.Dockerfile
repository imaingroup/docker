FROM node:14-alpine

RUN apk update && apk upgrade && apk add alpine-sdk python2

RUN mkdir /frontend
WORKDIR /frontend
COPY git/frontend/ .
RUN cp env.override.js env.js && \
    npm install && \
    npm run build
