# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git zip
```

## Get docker.
```
    mkdir imaingroup && cd imaingroup
    git clone git@gitlab.com:imaingroup/docker.git
```

##	Development environment.
```
    cd imaingroup
    git clone git@gitlab.com:imaingroup/backend.git
    git clone git@gitlab.com:imaingroup/frontend.git
    sudo sh -c "echo '127.0.0.1 insiderprotocol.local' >> /etc/hosts"
    sudo sh -c "echo '127.0.0.1 api.insiderprotocol.local' >> /etc/hosts"
    cd docker
    
    sudo docker-compose -f docker-compose.dev.yml up --build
```

##	Development init.
```
    sudo docker container exec -it $(sudo docker ps --format "{{.Names}}" | grep insider_dev_mysql) mysql -uroot -p
    CREATE USER 'insiderprotocol'@'%' IDENTIFIED BY 'password';
    GRANT ALL ON insiderprotocol.* TO 'insiderprotocol'@'%';
    exit;
    
    sudo docker exec -i $(sudo docker ps --format "{{.Names}}" | grep insider_dev_mysql) sh -c 'exec mysql -uroot -p"password" insiderprotocol' < dump.sql
```

##	Development commands.
```
    sudo docker exec -i $(sudo docker ps --format "{{.Names}}" | grep insider_dev_mysql) sh -c 'exec mysqldump -uroot -p"password" insiderprotocol' > dump.sql
```

##	Production environment: before the first launch.
```
    cd imaingroup
    
    mkdir -p secrets/main/redis/
    echo "password" > secrets/main/redis/password #set your password
    
    mkdir -p secrets/main/mysql
    echo 'password' > secrets/main/mysql/mysql_root_password
    
    mkdir -p secrets/main/ssl
    cp /ssl/main/ssl.crt secrets/main/ssl/nginx/ssl.crt
    cp /ssl/main/ssl.key secrets/main/ssl/nginx/ssl.key
    
    mkdir -p secrets/main/nginx
    cp docker/config/cloudflare_whitelist.conf secrets/main/nginx/cloudflare_whitelist.conf
    
    mkdir -p secrets/main/app/
    cp docker/git/backend/.env.example secrets/main/app/.env #than change all passwords and other accesses.
    
    cd docker
    sudo docker-compose -f build.yml build insider_mysql
    sudo docker-compose -f stack.yml run insider_mysql
    
    cd imaingroup
    
    sudo docker container exec -it container_id mysql -uroot -p
    CREATE USER 'insiderprotocol'@'%' IDENTIFIED BY 'password';
    GRANT ALL ON insiderprotocol.* TO 'insiderprotocol'@'%';
    exit;
    
    sudo docker exec -i container_id sh -c 'exec mysql -uinsiderprotocol -p"password" insiderprotocol' < dump.sql
    
    sudo mkdir -p data_main/logs/backend/
    sudo chown -R 82:82 data_main/logs/backend/
    sudo chmod -R 755 data_main/logs/backend/
    
    sudo mkdir -p data_main/app/storage/
    sudo chown -R 82:82 data_main/app/storage/
    sudo chmod -R 755 data_main/app/storage/
    
    sudo docker container stop container_id
```

##	Production deploy.
```
    cd docker
    
    #test
    sudo docker-compose --env-file ./config/env/.env.test -f build.yml build
    sudo docker stack deploy -c stack.yml -c stack.prod.yml insider 
    
    #prod
    sudo docker-compose --env-file ./config/env/.env.prod -f build.yml build
    sudo docker stack deploy -c stack.yml -c stack.prod.yml insider 
    
    sudo docker service update --replicas 1 insider_insider_cron
    sudo docker service update --replicas 1 insider_insider_queue
    sudo docker service update --replicas 1 insider_insider_fpm
    
    sudo docker service update --publish-add published=80,target=80,mode=host --publish-add published=443,target=443,mode=host insider_insider_nginx
```

##	Production update: example.
```
    #rebuild.
    
    #sudo docker service update --force --image insider_nginx:latest insider_insider_nginx
    sudo docker service update --publish-rm published=443,target=443,mode=host --publish-add published=443,target=5443,mode=host insider_insider_nginx
    sudo docker exec -i $(sudo docker ps --format "{{.Names}}" | grep insider_cron) php artisan down
    #wait stopped all cron
    sudo docker service update --replicas 0 insider_insider_cron

    sudo docker service update --force --image insider_ethereum:latest insider_insider_ethereum
    sudo docker service update --force --image insider_fpm:latest insider_insider_fpm
    sudo docker exec -i $(sudo docker ps --format "{{.Names}}" | grep insider_fpm) php artisan migrate
        
    sudo docker service update --replicas 1 --force --image insider_cron:latest insider_insider_cron
    sudo docker exec -i $(sudo docker ps --format "{{.Names}}" | grep insider_cron) php artisan up
    sudo docker service update --publish-rm published=443,target=5443,mode=host --publish-add published=443,target=443,mode=host insider_insider_nginx
```

##	Production update: update secrets.
```
    #update secret file.
    sudo docker service update --publish-rm published=443,target=443,mode=host --publish-add published=443,target=5443,mode=host insider_insider_nginx

    #enable maintenance mode.
    sudo docker service rm insider_insider_cron
    sudo docker service rm insider_insider_fpm
    sudo docker secret rm insider_app_env && sudo docker secret create --label "com.docker.stack.namespace=insider" insider_app_env ./secrets/main/app/.env
    sudo docker secret rm insider_cloudflare_whitelist.conf && sudo docker secret create --label "com.docker.stack.namespace=insider" insider_cloudflare_whitelist.conf ./secrets/main/nginx/cloudflare_whitelist.conf

    sudo docker stack deploy -c stack.yml -c stack.prod.yml insider
    sudo docker service update --publish-rm published=443,target=5443,mode=host --publish-add published=443,target=443,mode=host insider_insider_nginx
    #than cache clear in contianer.
    #add auto cache clear in containers.
```

##	Production compose (test mode).
```
    cd docker
    sudo docker-compose -f stack.yml -f stack.prod.yml up
```

## Configure db backups.

insert to crontab "*/30 * * * * cd /var/www/imaingroup/docker/scripts/ && bash backup.bash"
